Addax
=====

This is a text based feed reader intended to be simple to use while
remaining relatively powerful and flexible to allow fast skimming
through many feeds.
