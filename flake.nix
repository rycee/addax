{
  description = "A simple feed reader.";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    brick-widgets-pandoc.url =
      "git+https://git.sr.ht/~rycee/brick-widgets-pandoc";
  };

  outputs = { self, brick-widgets-pandoc, flake-utils, nixpkgs }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        hpkgs = pkgs.haskell.packages.ghc8107.override {
          overrides = self: super: {
            brick-widgets-pandoc =
              brick-widgets-pandoc.defaultPackage.${system};
          };
        };

        args = {
          name = "addax";
          root = pkgs.nix-gitignore.gitignoreSource [ ] ./.;
        };
      in {
        defaultPackage = hpkgs.developPackage args;

        devShell = hpkgs.developPackage (args // {
          returnShellEnv = true;
          modifier = drv:
            pkgs.haskell.lib.addBuildTools drv (with hpkgs; [
              cabal-fmt
              cabal-install
              cabal2nix
              haskell-language-server
              nixfmt
            ]);
        });
      });
}
