{ mkDerivation, async, base-noprelude, brick, brick-widgets-pandoc
, bytestring, config-ini, containers, data-default, directory, feed
, filepath, hspec, http-client, http-client-tls, http-types, lens
, lib, monad-logger, mtl, network-uri, open-browser
, optparse-applicative, pandoc, pandoc-types, parsec, persistent
, persistent-sqlite, persistent-template, process, relude
, resource-pool, tagsoup, text, time, transformers, vector, vty
, xdg-basedir, xml-types
}:
mkDerivation {
  pname = "addax";
  version = "0.1";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [
    async base-noprelude brick brick-widgets-pandoc bytestring
    config-ini containers data-default directory feed filepath
    http-client http-client-tls http-types lens monad-logger mtl
    network-uri open-browser optparse-applicative pandoc pandoc-types
    parsec persistent persistent-sqlite persistent-template process
    relude resource-pool tagsoup text time transformers vector vty
    xdg-basedir xml-types
  ];
  testHaskellDepends = [
    async base-noprelude brick brick-widgets-pandoc bytestring
    config-ini containers data-default directory feed filepath hspec
    http-client http-client-tls http-types lens monad-logger mtl
    network-uri open-browser optparse-applicative pandoc pandoc-types
    parsec persistent persistent-sqlite persistent-template process
    relude resource-pool tagsoup text time transformers vector vty
    xdg-basedir xml-types
  ];
  description = "A simple feed reader";
  license = lib.licenses.gpl3Plus;
}
